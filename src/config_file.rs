use serde::Deserialize;
use std::{fs::File, path::PathBuf};

use crate::get_project_file;

/// Name of the configuration file, which must reside inside the project config directory.
/// Example of full config file path on Linux: "$HOME/.config/lola/cli_config.yaml".
const LOLA_CONFIG_FILE: &str = "cli_config.yaml";

/// Result of the parse of the optional configuration file.
#[derive(Debug, Deserialize, Default)]
pub struct ConfigFile {
    #[serde(default)]
    pub import_paths: Vec<String>,
    #[serde(default)]
    pub imports: Vec<String>,
}

impl ConfigFile {
    /// Tries to parse the config file. If it is not found, return a default configuration.
    pub fn new() -> ConfigFile {
        if let Some((file, config_file)) = Self::open() {
            match serde_yaml::from_reader::<_, Self>(file) {
                Ok(config_file) => return config_file,
                Err(e) => eprintln!(
                    "Warning: unable to parse the config file '{}': {}",
                    config_file.display(),
                    e,
                ),
            };
        }
        Self::default()
    }

    fn open() -> Option<(File, PathBuf)> {
        let config_file = get_project_file(|d| d.config_dir(), LOLA_CONFIG_FILE)?;
        File::open(&config_file)
            .ok()
            .map(|file| (file, config_file))
    }
}
