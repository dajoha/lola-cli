/// An iterator which simplifies an `Option<Iterator>` into an `Iterator`.
pub struct OptIter<I: Iterator> {
    inner: Option<I>,
}

impl<I: Iterator> OptIter<I> {
    pub fn new(inner: Option<I>) -> Self {
        Self { inner }
    }
}

impl<I: Iterator> Iterator for OptIter<I> {
    type Item = <I as Iterator>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.inner {
            Some(iter) => iter.next(),
            None => None,
        }
    }
}
