use directories::ProjectDirs;
use std::path::{Path, PathBuf};

use anyhow::Result;
use lola::context::ContextRef;
use rustyline::error::ReadlineError;

use cli_config::Config;
use config_file::ConfigFile;
use eval::create_context;
use interactive::{process_interactive_line, Readline};

mod cli_config;
mod config_file;
mod eval;
mod formatted_value;
mod interactive;
#[cfg(feature = "readline-history")]
mod readline_history;
mod util;

/// Retrieves a project directory.
pub fn get_project_file<F>(dir_getter: F, filename: &str) -> Option<PathBuf>
where
    F: Fn(&ProjectDirs) -> &Path,
{
    ProjectDirs::from("", "", "lola").map(|dirs| {
        let filename = Path::new(filename);
        [dir_getter(&dirs), filename].iter().collect::<PathBuf>()
    })
}

/// Starts the interactive shell.
fn start_shell(cxt: &mut ContextRef, colored: bool) {
    let mut readline = Readline::new(cxt);

    // Main loop:
    loop {
        let line = readline.read();
        match line {
            Ok(line) => {
                #[cfg(feature = "readline-history")]
                readline.add_history_entry(line.as_str());

                process_interactive_line(line.as_str(), cxt, colored);
            }
            Err(ReadlineError::Eof) | Err(ReadlineError::Interrupted) => {
                println!("Bye");
                break;
            }
            Err(err) => {
                println!("Error: {}", err);
                break;
            }
        }
    }
}

pub fn real_main() -> Result<()> {
    let config = Config::new();
    let config_file = ConfigFile::new();
    let colored = config.color_mode.should_we_colorize();
    let (mut container, mut scope, last_output) = create_context(&config, &config_file, colored)?;
    let mut cxt = ContextRef::new(&mut container, &mut scope);

    if config.run_interactive_shell {
        start_shell(&mut cxt, colored);
    } else if let Some(output) = last_output {
        println!("{}", output);
    }

    Ok(())
}

fn main() {
    if let Err(err) = real_main() {
        eprintln!("{}", err);
        std::process::exit(1);
    }
}
